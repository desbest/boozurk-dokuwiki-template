# Boozurk dokuwiki template

* Based on a wordpress theme
* Designed by [TwoBeers Crew](https://wordpress.org/themes/boozurk/)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:boozurk)

![boozurk basic theme screenshot](https://i.imgur.com/j2kioih.png)