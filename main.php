<?php
/**
 * DokuWiki Boozurk Template
 * Based on the starter template and a wordpress theme of the same name
 *
 * @link     http://dokuwiki.org/template:boozurk
 * @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */
header('X-UA-Compatible: IE=edge,chrome=1');

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <!-- <script defer type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script> -->
     
    <?php tpl_includeFile('meta.html') ?>
</head>

<body id="dokuwiki__top" class="site <?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>">

    <?php //boozurk_hook_body_top(); ?>
    <div id="secondary1_wrap" class="menu-social-media-links-container">
        <ul id="secondary1" class="nav-menu l_tinynav1">
            <li class="menu-item"><a href="#">Menu option 1</a></li>
            <li class="menu-item"><a href="#">Menu option 2</a></li>
            <li class="menu-item"><a href="#">Menu option 3</a></li>
            <li class="menu-item"><a href="#">Menu option 4</a></li>
        </ul>
    </div>

        <div id="main">

            <div id="fixed-bg"></div>

            <div id="content">

                <?php //boozurk_hook_header_before(); ?>
                <div id="head_wrap"></div>

                <?php tpl_includeFile('header.html') ?>
                <div id="head">

                    <?php //boozurk_hook_header_top(); ?>

                    <?php //echo boozurk_get_header(); ?>
                    <h1><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></h1>
                    <!-- <h1 class="hide_if_no_print"><a href="">site name here</a></h1> -->

                    <?php if ($conf['tagline']): ?>
                        <p class="claim"><?php echo $conf['tagline'] ?></p>
                    <?php endif ?>

                    <ul class="a11y skip">
                    <li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
                    </ul>
                      <?php /* how to insert logo instead (if no CSS image replacement technique is used):
                        upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:
                        tpl_link(wl(),'<img src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
                    <!-- return apply_filters( 'boozurk_filter_header', $header ); -->

                    <?php //boozurk_hook_header_bottom(); ?>

                </div>

                <?php //boozurk_hook_header_after(); ?>
                <ul id="mainmenu" class="nav-menu l_tinynav2">
                  <!-- SITE TOOLS -->
                    <h3 class="a11y"><?php echo $lang['site_tools'] ?></h3>
                        <li class="selected mobile"><a href="#">Main menu</a></li>
                        <?php tpl_toolsevent('sitetools', array(
                            'recent'    => tpl_action('recent', 1, 'li', 1),
                            'media'     => tpl_action('media', 1, 'li', 1),
                            'index'     => tpl_action('index', 1, 'li', 1),
                        )); ?>
                </ul>

                <div id="breadcrumb-wrap">
                    <a class="item-home" rel="nofollow" href="<?php echo DOKU_BASE; ?>"><i class="icon-home"></i></a>
                    <!-- BREADCRUMBS -->
                    <div id="bz-breadcrumb">
                    <?php if($conf['breadcrumbs']){ ?>
                        <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
                    <?php } ?>
                    <?php if($conf['youarehere']){ ?>
                        <div class="breadcrumbs"><?php tpl_youarehere() ?></div>
                    <?php } ?>
                    <br class="fixfloat">
                    </div>
                </div>

                <div id="posts_content">

                    <!-- <div class="nav-single fixfloat">
                    <span class="nav-previous ">
                    <i class="icon-angle-left"></i>
                    <a rel="prev" href="http://localhost/wordpress/hello-world/" title="Previous Post: Hello world!">
                    <i class="tb-thumb-format icon-32 standard"></i> <span>Hello world!</span>
                    </a>
                    </span>
                    </div> -->
                    <!-- #nav-single -->

                    <div class="post-9 post type-post status-publish format-standard has-post-thumbnail hentry category-uncategorized post-element" id="post-9">

                        <div class="post_meta_container">
                            <a class="pmb_format" href="http://localhost/wordpress/test-post/" rel="bookmark"><i class="icon-placeholder"></i></a>
                            <!-- <a href="http://localhost/wordpress/test-post/#comments" class="pmb_comm" title="Comments on Test post">2</a> -->
                            <!-- <a class="post-edit-link" href="http://localhost/wordpress/wp-admin/post.php?post=9&amp;action=edit"><i class="icon icon-pencil"></i></a> -->
                        </div>

                        <!-- <h2 class="storytitle">Test post</h2> -->

                        <div class="storycontent">
                            <!-- ********** CONTENT ********** -->
                                <?php tpl_flush() /* flush the output buffer */ ?>
                                <?php tpl_includeFile('pageheader.html') ?>

                                <?php html_msgarea() /* occasional error and info messages on top of the page */ ?>

                                <div class="page">
                                    <!-- wikipage start -->
                                    <?php tpl_content() /* the main content */ ?>
                                    <!-- wikipage stop -->
                                    <div class="clearer"></div>
                                </div>

                                <?php tpl_flush() ?>
                                <?php tpl_includeFile('pagefooter.html') ?>
                            <!-- /content -->
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque, eros a gravida dignissim, metus mauris commodo velit, sed consectetur odio sapien sed tortor. Sed fringilla ullamcorper felis, quis suscipit eros semper sit amet. Donec rhoncus ipsum quis nulla aliquam vehicula. Donec sed aliquam ligula. Nulla laoreet quam tellus, quis tincidunt enim lobortis sed. Aenean vel venenatis tortor. Vestibulum mollis placerat elit egestas luctus. Vestibulum mattis non lorem consequat venenatis. Vestibulum suscipit lacinia ante ut efficitur. Fusce ut mattis lorem. Morbi condimentum, risus vitae accumsan iaculis, enim nunc luctus lacus, consectetur sollicitudin dui metus vitae augue.</p> -->
                        </div>

                        <div class="fixfloat"></div>

                    </div>

                    <!-- begin comments -->
                    <!-- <div id="comments">
                    2 Comments<span class="hide_if_print"> - <a href="#respond" title="Leave a comment">Leave a comment</a></span>
                    </div>




                    <ol id="commentlist">
                    <li class="comment byuser comment-author-desbest bypostauthor even thread-even depth-1 parent" id="comment-2">
                    <div id="div-comment-2" class="comment-body">
                    <div class="comment-author vcard">
                    <img alt="" src="http://2.gravatar.com/avatar/bcdd9f5447f1a3f18cefdb8c3f6b4dab?s=32&amp;d=mm&amp;r=g" srcset="http://2.gravatar.com/avatar/bcdd9f5447f1a3f18cefdb8c3f6b4dab?s=64&amp;d=mm&amp;r=g 2x" class="avatar avatar-32 photo" width="32" height="32">            <cite class="fn">desbest</cite> <span class="says">says:</span>     </div>

                    <div class="comment-meta commentmetadata"><a href="http://localhost/wordpress/test-post/#comment-2">
                    May 31, 2020 at 8:02 pm             </a>
                    &nbsp;&nbsp;<a class="comment-edit-link" href="http://localhost/wordpress/wp-admin/comment.php?action=editcomment&amp;c=2"><i class="icon icon-pencil"></i></a>     </div>

                    <p>parent comment</p>

                    <div class="reply"><a rel="nofollow" class="comment-reply-link" href="http://localhost/wordpress/test-post/?replytocom=2#respond" data-commentid="2" data-postid="9" data-belowelement="div-comment-2" data-respondelement="respond" aria-label="Reply to desbest" title="Reply to comment"><i class="icon icon-share-alt"></i></a></div>
                    </div>
                    <ul class="children">
                    <li class="comment byuser comment-author-desbest bypostauthor odd alt depth-2" id="comment-3">
                    <div id="div-comment-3" class="comment-body">
                    <div class="comment-author vcard">
                    <img alt="" src="http://2.gravatar.com/avatar/bcdd9f5447f1a3f18cefdb8c3f6b4dab?s=32&amp;d=mm&amp;r=g" srcset="http://2.gravatar.com/avatar/bcdd9f5447f1a3f18cefdb8c3f6b4dab?s=64&amp;d=mm&amp;r=g 2x" class="avatar avatar-32 photo" width="32" height="32">            <cite class="fn">desbest</cite> <span class="says">says:</span>     </div>

                    <div class="comment-meta commentmetadata"><a href="http://localhost/wordpress/test-post/#comment-3">
                    May 31, 2020 at 8:03 pm             </a>
                    &nbsp;&nbsp;<a class="comment-edit-link" href="http://localhost/wordpress/wp-admin/comment.php?action=editcomment&amp;c=3"><i class="icon icon-pencil"></i></a>     </div>

                    <p>child comment</p>

                    <div class="reply"><a rel="nofollow" class="comment-reply-link" href="http://localhost/wordpress/test-post/?replytocom=3#respond" data-commentid="3" data-postid="9" data-belowelement="div-comment-3" data-respondelement="respond" aria-label="Reply to desbest" title="Reply to comment"><i class="icon icon-share-alt"></i></a></div>
                    </div> -->
                    <!-- </li> --><!-- #comment-## -->
                    <!-- </ul> --><!-- .children -->
                    <!-- </li> --><!-- #comment-## -->
                    <!-- </ol> -->



                   <!--  <div id="respond" class="comment-respond">
                    <h3 id="reply-title" class="comment-reply-title">Leave a Reply <small><a rel="nofollow" id="cancel-comment-reply-link" title="Cancel reply" href="/wordpress/test-post/#respond" style="display:none;"><i class="icon-remove-circle"></i></a></small><small> - <a id="bz-quotethis" href="#" onclick="boozurkScripts.quote_this(); return false" title="Add selected text as a quote">Quote</a></small></h3><form action="http://localhost/wordpress/wp-comments-post.php" method="post" id="commentform" class="comment-form"><p class="logged-in-as"><a href="http://localhost/wordpress/wp-admin/profile.php" aria-label="Logged in as desbest. Edit your profile.">Logged in as desbest</a>. <a href="http://localhost/wordpress/wp-login.php?action=logout&amp;redirect_to=http%3A%2F%2Flocalhost%2Fwordpress%2Ftest-post%2F&amp;_wpnonce=41693b0318">Log out?</a></p><p class="comment-form-comment"><textarea id="comment" name="comment" cols="45" rows="7" aria-required="true"></textarea></p><p class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Say It!"> <input type="hidden" name="comment_post_ID" value="9" id="comment_post_ID">
                    <input type="hidden" name="comment_parent" id="comment_parent" value="0">
                    </p><p style="display: none;"><input type="hidden" id="akismet_comment_nonce" name="akismet_comment_nonce" value="d9536f24f2"></p><input type="hidden" id="_wp_unfiltered_html_comment_disabled" name="_wp_unfiltered_html_comment" value="c35f4577d7"><script>(function(){if(window===window.parent){document.getElementById('_wp_unfiltered_html_comment_disabled').name='_wp_unfiltered_html_comment';}})();</script>
                    <textarea name="ak_hp_textarea" cols="45" rows="8" maxlength="100" style="display: none !important;"></textarea><input type="hidden" id="ak_js" name="ak_js" value="1610451848077"></form>  </div> -->
                    <!-- #respond -->

                    <!-- <br class="fixfloat"> -->

                    <!-- end comments -->

                </div><!-- end #posts_content -->

                <div id="sidebars"></div>

            </div><!-- close #main content -->

            <div id="sidebars">

                <div class="sidebar scroll" id="sidebar-primary"><div class="viewport"><div class="overview">

                <div id="search-10" class="widget widget_search">
                
                <!-- <form role="search" method="get" class="searchform" action="http://localhost/wordpress/">
                <div class="searchform-wrap">
                <label class="screen-reader-text" for="s">Search for:</label>
                <input type="text" value="" name="s" id="s">
                <button type="submit">
                <i class="icon-search"></i>
                </button>
                </div>
                </form> -->

                <?php tpl_searchform() ?>
                </div>

                <div id="categories-11" class="widget widget_categories"><div class="w_title">User Tools</div>        <ul>
                <li class="cat-item cat-item-1"><a href="http://localhost/wordpress/category/uncategorized/">Uncategorized</a></li>
                 <!-- USER TOOLS -->
                <?php if ($conf['useacl'] && $showTools): ?>
                <h3 class="a11y"><?php echo $lang['user_tools'] ?></h3>
                    <?php
                        if (!empty($_SERVER['REMOTE_USER'])) {
                            echo '<li class="user">';
                            tpl_userinfo(); /* 'Logged in as ...' */
                            echo '</li>';
                        }
                    ?>
                    <?php /* the optional second parameter of tpl_action() switches between a link and a button,
                             e.g. a button inside a <li> would be: tpl_action('edit', 0, 'li') */
                    ?>
                    <?php tpl_toolsevent('usertools', array(
                        'admin'     => tpl_action('admin', 1, 'li', 1),
                        'userpage'  => _tpl_action('userpage', 1, 'li', 1),
                        'profile'   => tpl_action('profile', 1, 'li', 1),
                        'register'  => tpl_action('register', 1, 'li', 1),
                        'login'     => tpl_action('login', 1, 'li', 1),
                    )); ?>
                <?php endif ?>
                </ul>
                </div>      

             <!--  <div class="widget widget_recent_entries">      <div class="w_title">Site Tools</div>     <ul>
                 
                </ul>
                </div> -->

                <br class="fixfloat">

                </div></div></div>

                <?php //get_sidebar(); // show primary widgets area ?>

                <?php //get_sidebar( 'secondary' ); // show secondary widgets area ?>

                <div class="sidebar tinyscroll" id="sidebar-secondary">
                    <div class="viewport" style="height: 377px;">
                        <div class="scrollbar" style="height: 377px;">
                            <div class="track" style="height: 377px;"><div class="thumb" style="top: 0px; height: 357.108px;"><div class="end"></div></div></div></div>
                        <div class="overview" style="top: 0px;">

                            <!-- <div class="bz-description">Just another WordPress site</div> -->
                            <!-- here should be the secondary widget area -->

                            <div id="fixed-widget-area">

                            <!-- ********** ASIDE ********** -->
                            <?php if ($showSidebar): ?>
                                <aside id="writtensidebar" class="widget">
                                    <?php tpl_includeFile('sidebarheader.html') ?>
                                    <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
                                    <?php tpl_includeFile('sidebarfooter.html') ?>
                                    <div class="clearer"></div>
                                </aside><!-- /aside -->
                            <?php endif; ?>

                            <!-- <div id="bz-clean-archives-2" class="widget tb_clean_archives">
                                <div class="w_title">Archives (boozurk)</div><ul class="tb-clean-archives">
                                    <li><a class="year-link" href="http://localhost/wordpress/2020/">2020</a><a class="month-link" href="http://localhost/wordpress/2020/02/">2</a></li>
                                    <li><a class="year-link" href="http://localhost/wordpress/2019/">2019</a><a class="month-link" href="http://localhost/wordpress/2019/04/">4</a></li>
                                </ul>
                            </div> -->

                            <!-- <div id="archives-5" class="widget widget_archive"><div class="w_title">Archives</div>       <ul>
                            <li><a href="http://localhost/wordpress/2020/02/">February 2020</a></li>
                            <li><a href="http://localhost/wordpress/2019/04/">April 2019</a></li>
                            </ul>
                            </div>
                            <br class="fixfloat"> 

                            </div> --><!-- #fixed-widget-area -->

                </div></div></div>

            </div>

            <?php //boozurk_hook_footer_before(); ?>

            <div id="footer">

                <div id="navbuttons" class="fixed">

                <ul>
                      <!-- PAGE ACTIONS -->
                <?php if ($showTools): ?>
                    <h3 class="a11y"><?php echo $lang['page_tools'] ?></h3>
                    <?php tpl_toolsevent('pagetools', array(
                        'edit'      => tpl_action('edit', 1, 'li class=\'minibutton btn minib_home icon-edit\' original-title=\'Edit\'', 1),
                        'discussion'=> _tpl_action('discussion', 1, 'li class=\'minibutton icon-comment\'  original-title=\'Discuss\'', 1),
                        'revisions' => tpl_action('revisions', 1, 'li class=\'minibutton icon-time\'  original-title=\'Old versions\'', 1),
                        'backlink'  => tpl_action('backlink', 1, 'li class=\'minibutton icon-link\'  original-title=\'Backlinks\'', 1),
                        'subscribe' => tpl_action('subscribe', 1, 'li class=\'minibutton icon-eye-open\'  original-title=\'Subscribe\'', 1),
                        'revert'    => tpl_action('revert', 1, 'li class=\'minibutton icon-undo\'  original-title=\'Revert\'', 1),
                        'top'       => tpl_action('top', 1, 'li class=\'minibutton icon-arrow-up\'  original-title=\'Go to top\'', 1),
                    )); ?>
                <?php endif; ?>
                <!-- <li class="minibutton" title="Print"><a rel="nofollow" href="http://localhost/wordpress/test-post/?style=printme"><i class="icon-print"></i></a></li>
                <li class="minibutton" title="Leave a comment"><a href="#respond"><i class="icon-comment"></i></a></li>
                <li class="minibutton" title="Feed for comments on this post"><a href="http://localhost/wordpress/test-post/feed/ "><i class="icon-rss"></i></a></li>
                <li class="minibutton" title="Trackback URL"><a href="http://localhost/wordpress/test-post/trackback/" rel="trackback"><i class="icon-reply"></i></a></li>
                <li class="minibutton" title="Home"><a href="http://localhost/wordpress"><i class="icon-home"></i></a></li>
                <li class="minibutton minib_ppage" title="Previous Post: Hello world!"><a rel="prev" href="http://localhost/wordpress/hello-world/"><i class="icon-chevron-left"></i></a></li>
                <li class="minibutton" title="Top of page"><a href="#"><i class="icon-chevron-up"></i></a></li>
                <li class="minibutton" title="Bottom of page"><a href="#footer"><i class="icon-chevron-down"></i></a></li> -->
                </ul>

                </div>
                <ul id="secondary2" class="nav-menu l_tinynav3">
                <li class="menu-item"><a href="#">Menu option 1</a></li>
                <li class="menu-item"><a href="#">Menu option 2</a></li>
                <li class="menu-item"><a href="#">Menu option 3</a></li>
                <li class="menu-item"><a href="#">Menu option 4</a></li>
                </ul>

                <!-- here should be the footer widget area -->

                <div id="bz-credits">

                © 2021 <strong><?php echo $conf['title']; ?></strong>
                <br>Powered by <a target="_blank" href="http://wordpress.org/" title="WordPress">WordPress</a> and <a target="_blank" href="http://www.twobeers.net/" title="Visit theme authors homepage @ twobeers.net">Boozurk theme</a> and converted by <a href="http://desbest.com">desbest</a>
                <!-- <span class="hide_if_print"> - <a rel="nofollow" href="http://localhost/wordpress?mobile_override=mobile">Mobile View</a></span> -->

                <div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
                <?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>
                </div>

                <!-- 66 queries. 2.311 seconds. -->


                </div>
                <?php tpl_includeFile('footer.html') ?>
                <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>



            <!-- <div id="footer"> -->

                <?php //boozurk_hook_footer_top(); ?>

                <?php //get_sidebar( 'footer' ); // show footer widgets area ?>

                <!-- <div id="bz-credits"> -->

                    <?php //echo boozurk_get_credits(); ?>

                <!-- </div> -->

                <!-- <?php //echo get_num_queries(); ?> queries. <?php //timer_stop(1); ?> seconds. -->

                <?php //boozurk_hook_footer_bottom(); ?>

            </div><!-- close footer -->

            <?php //boozurk_hook_footer_after(); ?>

        </div><!-- close main -->

        <div id="print-links" class="hide_if_no_print"><a href="/"><?php //echo __('Close','boozurk'); ?></a><span class="hide-if-no-js"> | <a href="javascript:window.print()">Print</a></span></div>

        <?php //boozurk_hook_body_bottom(); ?>

        <?php //wp_footer(); ?>

    <?php /* with these Conditional Comments you can better address IE issues in CSS files,
             precede CSS rules by #IE8 for IE8 (div closes at the bottom) */ ?>
    <!--[if lte IE 8 ]><div id="IE8"><![endif]-->

    <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>


     <!-- due to the way dokuwiki buffers output, this javascript has to
            be before the </body> tag and not in the <head> -->
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/tinynav/tinynav.js"></script>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/calltinynav.js"></script>
    <script defer type="text/javascript" src="<?php echo tpl_basedir();?>/cooltips.js"></script>

</body>
</html>
